﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class IglooCinemachineLook : MonoBehaviour
{

    private CinemachineFreeLook freeLook;
    private float lastHeading = 0.0f;
    private float lastLookValue = 0.0f;
    float smoothTime = 0.01f;
    float yVelocity = 0.0f;
    float deadzone = 2.5f;


    void Start()
    {
        freeLook = GetComponent<CinemachineFreeLook>();
    }
    /*
    private void LateUpdate()
    {
        float newHeading = IglooInputXIMU.Heading;
        float lookValue = 0.0f;

        if (newHeading > (lastHeading + deadzone))
        {
            // newheading is more than the last heading + deadzone
            lookValue = Mathf.Lerp(lastLookValue, Mathf.Abs(newHeading - lastHeading), smoothTime * Time.deltaTime);

        }
        else if (newHeading < (lastHeading - deadzone))
        {
            // newheading is less than the last heading + deadzone (we are spinning the other way) so we also minus this value.
            lookValue = Mathf.Lerp(lastLookValue, Mathf.Abs(newHeading + lastHeading), smoothTime * Time.deltaTime);
        }
        else
        {
            // we are stuck in the dead zone, don't do anything.
            lookValue = Mathf.Lerp(lastLookValue, 0, smoothTime * Time.deltaTime);
        }

        // Set the last heading ready for the next update
        lastHeading = newHeading;
        // Set the current value to the camera system.
        freeLook.m_XAxis.m_InputAxisValue = lookValue;
        // set the last look value ready for the next go around
        lastLookValue = lookValue;
    }
    */

}

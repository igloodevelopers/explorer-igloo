﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckRenderTexture : MonoBehaviour
{
    private RenderTexture tex;

    // Start is called before the first frame update
    void Start()
    {
        // Create new camera textures
        tex = new RenderTexture(4000, 500, 24);
        GetComponent<Camera>().targetTexture = tex;
        if (GameObject.FindGameObjectWithTag("IglooUI"))
        {
            GameObject.FindGameObjectWithTag("IglooUI").GetComponent<Renderer>().materials[1].mainTexture = tex;
        }
        else InvokeRepeating("WaitForUI", 1.0f, 1.0f);
    }

    void WaitForUI()
    {
        if (GameObject.FindGameObjectWithTag("IglooUI"))
        {
            GameObject.FindGameObjectWithTag("IglooUI").GetComponent<Renderer>().materials[1].mainTexture = tex;
            CancelInvoke("WaitForUI");
        }
    }
}
